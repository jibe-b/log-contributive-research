# Votre point d'entrée pour les données de la recherche contributive


## Description

Cette application est un point d'entrée pour la contribution à des projets de recherche contributive.

L'utilisateur·rice peut consulter une liste de chaînes qui correspondent à des projets de recherche contributive. Sur la notice, des informations sont disponibles pour participer au projet correspondant, incluant un bouton/lien pour installer/lancer l'application de ce projet, ainsi qu'un (lien vers) un tutoriel pour débuter dans ce projet.

Un second volet permet de visualiser des jeux de données ayant été ouverts par ces projets de recherche contributive.

Un troisième volet met à disposition une timeline, qui a vocation à être fédérée avec les réseaux sociaux existants.



** **

## Raisons d'être

De nombreux projets de recherche contributive existent mais il est ardu d'être tenu à jour. Les projets de recherche contributive donnent beaucoup de leur énergie à trouver des contributeurs.

## Avancement

Cette application est en route vers l'état de minimum viable product.

## Feuille de route de développement



## Comment contribuer


